<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD</title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="body">
<a href="../home.html"><button class="btnExample btn-block" type="submit" value="Submit"/>Home</button></a>
<div class="container content panel panel-default">


    <div class="row">

        <div class="col-md-offset-2 col-md-8">

            <form action="store.php" method="post">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input name="first_name" type="text" class="form-control" id="first_name" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input name="last_name" type="text" class="form-control" id="last_name" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input name="email" type="email" class="form-control" id="email" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <label for="seip">SEIP ID</label>
                    <input name="seip" type="text" class="form-control" id="seip" placeholder="SEIP-ID">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>

            <a href="index.php" target="_blank"><button class="btnExample btn-block" type="submit" value="Submit"/>See Students List</button></a>

        </div>

    </div>

</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>