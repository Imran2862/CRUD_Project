
<?php

$db = new PDO('mysql:host=localhost; dbname=crud;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `students` WHERE id= ".$_GET['id'];

$stmt = $db->query($query);

$student = $stmt->fetch(PDO::FETCH_ASSOC);


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD</title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body>

<div class="container">

    <div class="row">
        <nav>
            <a href="index.php"><button class="btnExample btn-block" type="submit" value="Submit"/>All Students</button></a>

        </nav>

        <div class="col-md-offset-3 col-md-6 showfile ">

           <!-- <dd>
                <dt>ID</dt>
                <dd><?/*=$student['id'];*/?></dd>
                <dt>Full Name</dt>
                <dd><?/*=$student['first_name']." ".$student['last_name'];*/?></dd>
                <dt>E-mail</dt>
                <dd><?/*=$student['email'];*/?></dd>
                <dt>SEIP ID</dt>
                <dd><?/*=$student['seip'];*/?></dd>

            </dd>-->

            <table class="table table-bordered">
                <thead>
                <th>ID</th>
                <th>Full Name</th>
                <th>E-mail</th>
                <th>SEIP ID</th>
                </thead>
                <tbody>
                <tr>
                    <td><?=$student['id'];?></td>
                    <td><?=$student['first_name']." ".$student['last_name'];?></td>
                    <td><?=$student['email'];?></td>
                    <td><?=$student['seip'];?></td>
                </tr>
                </tbody>

            </table>



        </div>

    </div>

</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>