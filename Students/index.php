
<?php

$db = new PDO('mysql:host=localhost; dbname=crud;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `students` ORDER BY id DESC ";

$stmt = $db->query($query);

$students = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD</title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="body">

<a href="../home.html"><button class="btnExample btn-block" type="submit" value="Submit"/>Home</button></a

<div class="container ">

    <div class="row tablelist">


        <div class="col-md-offset-2 col-md-8">

            <table class="table table-bordered">

                <thead>

                <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>E-mail</th>
                <th>SEIP</th>
                <th>Actions</th>
                </tr>

                </thead>

                <tbody>
                <?php

                foreach($students as $student):


                ?>
                <tr>
                    <td><?= $student['id']?></td>
                    <td><?= $student['first_name']?></td>
                    <td><?= $student['last_name']?></td>
                    <td><?= $student['email']?></td>
                    <td><?= $student['seip']?></td>
                    <td>
                        <a href="show.php?id=<?= $student['id']?>"> Show</a> |
                        <a href="edit.php?id=<?= $student['id']?>"> Edit</a> |
                        <a href="delete.php?id=<?= $student['id']?>"> Delete</a>
                    </td>
                </tr>

                <?php
                endforeach;
                ?>
                </tbody>


            </table>

            <nav>
                <a href="create.php"><button class="btnExample btn-block" type="submit" value="Submit"/>Add New Student</button></a>

            </nav>

        </div>

    </div>

</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>