<?php

//db connection

$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
//build query

$query = "SELECT * FROM `courses` ORDER BY ID DESC ";

//build exec

$result = $db->query($query);

$results = $result->fetchAll(PDO::FETCH_ASSOC);

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="body">
<a href="../home.html"><button class="btnExample btn-block" type="submit" value="Submit"/>Home</button></a>

<div class="container">

    <div class="row tablelist">


        <div class="col-md-offset-3 col-md-6">

            <table class="table table-bordered">

                <thead>
                <tr>
                    <th>ID</th>
                    <th>Subject Code</th>
                    <th>Subject Title</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($results as $course):
                ?>
                <tr>
                    <td><?= $course['id']?></td>
                    <td><?= $course['subject_code']?></td>
                    <td><?= $course['subject_title']?></td>
                    <td><a href="show.php?id=<?= $course['id']?>">Show</a> |
                        <a href="edit.php?id=<?= $course['id']?>">Edit</a> |
                        <a href="delete.php?id=<?= $course['id']?>">Delete</a>
                    </td>
                </tr>
                <?php
                endforeach;
                ?>

                </tbody>
            </table>
            <nav>
                <a href="create.php"><button class="btnExample btn-block" type="submit" value="Submit"/>Add Course</button></a>

            </nav>
        </div>

    </div>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>