<?php

//db connection

$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
//build query

$query = "SELECT * FROM `courses` WHERE ID= ".$_GET['id'];

//build exec

$result = $db->query($query);

$results = $result->fetch(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="body">

<div class="container">

    <div class="row">

        <nav>
            <a href="index.php"><button class="btnExample btn-block" type="submit" value="Submit"/>All Courses</button></a>

        </nav>


        <div class="col-md-offset-3 col-md-6 showfile">



            <table class="table table-bordered">
                <thead>
                <th>ID</th>
                <th>Course Code</th>
                <th>Course Title</th>

                </thead>
                <tbody>
                <tr>
                    <td><?=$results['id'];?></td>

                    <td><?=$results['subject_code'];?></td>
                    <td><?=$results['subject_title'];?></td>
                </tr>
                </tbody>

            </table>
        </div>

    </div>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
